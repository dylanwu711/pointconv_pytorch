import torch
import torch.nn as nn
import torch.nn.init as init
import torch.nn.functional as F 

from pointconv_util import * 

base_dim = 64
num_level = 4
nsample = 32
feat_dim = [base_dim * (i + 1) for i in range(num_level)]
num_points = [2048, 1024, 512, 256]

USE_MLP = True

class PointConvSegNet(nn.Module):

    def __init__(self, num_classes = 50):
        super(PointConvSegNet, self).__init__()

        self.num_classes = num_classes

        self.total_level = num_level

        weightnet = 16 

        self.relu = torch.nn.ReLU(inplace=True)

        # pointwise_encode 
        self.pw_conv1 = nn.Conv1d(3, base_dim, 1)
        self.pw_bn1 = nn.BatchNorm1d(base_dim)
        self.pw_conv2 = nn.Conv1d(base_dim, base_dim, 1)
        self.pw_bn2 = nn.BatchNorm1d(base_dim)
        self.pw_conv3 = nn.Conv1d(base_dim, base_dim, 1)
        self.pw_bn3 = nn.BatchNorm1d(base_dim)

        self.pointconv = nn.ModuleList()
        self.pointconv_mlp = nn.ModuleList() 

        # encoder
        last_ch = feat_dim[0]
        for l, cur_ch in enumerate(feat_dim[1:], 1):
            in_ch = last_ch
            out_ch = cur_ch 

            self.pointconv.append(PointConvD(num_points[l], nsample, in_ch, out_ch, weightnet))

            if USE_MLP:
                mlp = [out_ch, out_ch]
            else:
                mlp = []

            self.pointconv_mlp.append(MLP(mlp))

            last_ch = out_ch

        self.pointdeconv = nn.ModuleList() 
        self.pointdeconv_mlp = nn.ModuleList() 

        # decoder 
        for l in range(len(feat_dim) - 2, -1, -1):
            in_ch = feat_dim[l + 1]
            out_ch = feat_dim[l]

            self.pointdeconv.append(PointConvTranspose(nsample, in_ch, out_ch, weightnet))

            mlp = [out_ch * 2, out_ch]

            self.pointdeconv_mlp.append(MLP(mlp))

        self.fc1 = nn.Conv1d(base_dim, num_classes, 1)

    def forward(self, feat, xyz):
        """
            feat: B, 3/6, N
            xtz: B, 3, N
        """
        # encode pointwise info 
        pointwise_feat = self.relu(self.pw_bn1(self.pw_conv1(feat)))
        pointwise_feat = self.relu(self.pw_bn2(self.pw_conv2(pointwise_feat)))
        pointwise_feat = self.relu(self.pw_bn3(self.pw_conv3(pointwise_feat)))

        feat_list = [pointwise_feat]
        points_list = [xyz]
        for i, pointconv in enumerate(self.pointconv):
            dense_xyz = points_list[-1] 
            dense_feat = feat_list[-1]
            sparse_xyz, sparse_feat, _ = pointconv(dense_xyz, dense_feat)

            sparse_feat = self.pointconv_mlp[i](sparse_feat)

            feat_list.append(sparse_feat)
            points_list.append(sparse_xyz)

        for i, pointdeconv in enumerate(self.pointdeconv):
            cur_level = self.total_level -2 - i

            dense_xyz = points_list[cur_level]
            dense_feat = feat_list[cur_level]

            dense_feat_from_sparse = pointdeconv(sparse_xyz, dense_xyz, sparse_feat)

            new_dense_feat = torch.cat([dense_feat_from_sparse, dense_feat], axis = 1)

            dense_feat = self.pointdeconv_mlp[i](new_dense_feat)

            feat_list[cur_level] = dense_feat

            sparse_xyz, sparse_feat = dense_xyz, dense_feat

        fc = self.fc1(sparse_feat)
        return fc

if __name__ == "__main__":

    N = 2048
    xyz = torch.ones(1, 3, N).cuda()
    feat = torch.ones(1, 6, N).cuda()

    pointconvseg = PointConvSegNet(num_classes=50).cuda()

    pred = pointconvseg(feat, xyz)

    print(pred.shape)        
            








