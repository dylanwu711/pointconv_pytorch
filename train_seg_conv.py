import argparse
import os

from comet_ml import Experiment
import torch
import torch.nn.parallel
import torch.utils.data
import torch.nn.functional as F
from data_utils.ShapeNetPartDataLoader import PartNormalDataset
import datetime
import logging
from pathlib import Path
from tqdm import tqdm
from utils.utils import test, save_checkpoint
from models import PointConvSegNet
import numpy as np
import random

def parse_args():
    '''PARAMETERS'''
    parser = argparse.ArgumentParser('PointConv')
    parser.add_argument('--batchsize', type=int, default=16, help='batch size in training')
    parser.add_argument('--epoch',  default=400, type=int, help='number of epoch in training')
    parser.add_argument('--learning_rate', default=0.001, type=float, help='learning rate in training')
    parser.add_argument('--gpu', type=str, default='0', help='specify gpu device')
    parser.add_argument('--workers', type=int, help='number of data loading workers', default=4)
    parser.add_argument('--optimizer', type=str, default='Adam', help='optimizer for training')
    parser.add_argument('--pretrain', type=str, default=None,help='whether use pretrain model')
    parser.add_argument('--decay_rate', type=float, default=1e-4, help='decay rate of learning rate')
    parser.add_argument('--model_name', default='pointconv', help='model name')
    return parser.parse_args()

def main(args):
    '''HYPER PARAMETER'''
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    datapath = './shapenet_partseg_normal/'

    experiment = Experiment(api_key="Nb3ID7sUOyPiRz0LitWp2XhRL", project_name="shapenet-part-seg-basedim-64", workspace="dylanwusee",
                            log_code=False, log_graph=False, auto_output_logging="simple", log_env_details=False,
                            auto_metric_logging=False, disabled=False)
    experiment.log_parameters(vars(args))
    experiment.set_filename(args.model_name)
    experiment.set_name(args.model_name)

    args.manualSeed = random.randint(1, 10000)  # fix seed
    print("Random Seed: ", args.manualSeed)
    random.seed(args.manualSeed)
    torch.manual_seed(args.manualSeed)

    '''CREATE DIR'''
    experiment_dir = Path('./experiment/')
    experiment_dir.mkdir(exist_ok=True)
    file_dir = Path(str(experiment_dir) + '/%s-ShapeNet-PartSeg-'%args.model_name + str(datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')))
    file_dir.mkdir(exist_ok=True)
    checkpoints_dir = file_dir.joinpath('checkpoints/')
    checkpoints_dir.mkdir(exist_ok=True)
    log_dir = file_dir.joinpath('logs/')
    log_dir.mkdir(exist_ok=True)

    '''LOG'''
    args = parse_args()
    logger = logging.getLogger(args.model_name)
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(str(log_dir) + 'train_%s_seg.txt'%args.model_name)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.info('---------------------------------------------------TRANING---------------------------------------------------')
    logger.info('PARAMETER ...')
    logger.info(args)

    '''DATA LOADING'''
    logger.info('Load dataset ...')
    dataset = PartNormalDataset(root=datapath, classification=False)
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=args.batchsize, shuffle=True, num_workers=int(args.workers))

    test_dataset = PartNormalDataset(root=datapath, classification=False, split='test', data_augmentation=False)
    testdataloader = torch.utils.data.DataLoader(test_dataset, batch_size=args.batchsize, shuffle=True, num_workers=int(args.workers))

    logger.info("The number of training data is: %d", len(dataset))
    logger.info("The number of test data is: %d", len(test_dataset))
    print("The number of training data is:", len(dataset))
    print("The number of test data is:", len(test_dataset))
    num_classes = 50
    logger.info("total number of classes: %d", num_classes)
    print("total number of classes: ", num_classes)

    blue = lambda x: '\033[94m' + x + '\033[0m'

    '''MODEL LOADING'''
    model = PointConvSegNet(num_classes).cuda()

    if args.optimizer == 'SGD':
        optimizer = torch.optim.SGD(model.parameters(), lr=args.learning_rate, momentum=0.9)
    elif args.optimizer == 'Adam':
        optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate, betas=(0.9, 0.999), eps=1e-08, weight_decay=args.decay_rate)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=60, gamma=0.5)
    LEARNING_RATE_CLIP = 1e-5

    if args.pretrain is not None:
        print('Use pretrain model...')
        logger.info('Use pretrain model')
        checkpoint = torch.load(args.pretrain)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        print("=> loaded checkpoint '{}' (epoch {})".format(args.pretrain, checkpoint['epoch']))
    else:
        print('No existing model, starting training from scratch...')
        start_epoch = 0

    global_epoch = start_epoch
    global_step = start_epoch * len(dataloader)
    best_tst_accuracy = 0.0

    seg_classes = dataset.seg_classes
    seg_label_to_cat = {} # {0:Airplane, 1:Airplane, ...49:Table}
    for cat in seg_classes.keys():
        for label in seg_classes[cat]:
            seg_label_to_cat[label] = cat   

    '''TRANING'''
    logger.info('Start training...')
    for epoch in range(start_epoch, args.epoch):
        print('Epoch %d (%d/%s):' % (global_epoch + 1, epoch + 1, args.epoch))
        logger.info('Epoch %d (%d/%s):', global_epoch + 1, epoch + 1, args.epoch)

        scheduler.step()
        lr = max(optimizer.param_groups[0]['lr'], LEARNING_RATE_CLIP)
        print('Learning rate:%f'%lr)
        logger.info('Learning rate:%f'%lr)
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

        train_shape_ious = {cat:[] for cat in seg_classes.keys()}
        total_correct = 0
        total_seen = 0
        total_loss = 0
        max_label = 0

        for batch_id, data in tqdm(enumerate(dataloader, 0), total=len(dataloader), smoothing=0.9):
            points, _,target = data
            points = points.transpose(2, 1)
            batch_size, _, num_points = points.shape
            points, target = points.cuda(), target.cuda()
            xyz = points[:, :3, :]
            optimizer.zero_grad()
            model = model.train()
            pred = model(points, xyz)
            loss = F.nll_loss(F.log_softmax(pred, dim = 1), target)

            loss.backward()
            optimizer.step()
            global_step += 1

            pred_np = pred.cpu().data.numpy()
            target_np = target.cpu().data.numpy()
            loss = loss.cpu().data.numpy()

            # Constrain pred to the groundtruth classes (selected by seg_classes[cat])
            cur_pred_logits = pred_np
            cur_pred_val = np.zeros((batch_size, num_points)).astype(np.int32)
            for i in range(batch_size):
                cat = seg_label_to_cat[target_np[i, 0]]
                logits = cur_pred_logits[i]
                cur_pred_val[i] = np.argmax(logits[seg_classes[cat], :], 0) + seg_classes[cat][0]

            correct = np.sum(cur_pred_val == target_np)
            total_correct += correct
            total_seen += (batch_size * num_points)
            total_loss += loss * (batch_size * num_points)

            for i in range(target_np.shape[0]):
                segp = cur_pred_val[i]
                segl = target_np[i]
                cat = seg_label_to_cat[segl[0]]
                part_ious = [0.0 for _ in range(len(seg_classes[cat]))]
                for l in seg_classes[cat]:
                    if (np.sum(segl == l) == 0) and (np.sum(segp == l) == 0):
                        part_ious[l - seg_classes[cat][0]] = 1.0
                    else:
                        part_ious[l-seg_classes[cat][0]] = np.sum((segl==l) & (segp==l)) / float(np.sum((segl==l) | (segp==l)))
                train_shape_ious[cat].append(np.mean(part_ious))

            experiment.log_metric(name="train/mAcc_step", value=correct / (args.batchsize * num_points), step=global_step)
            experiment.log_metric(name="train/mLoss_step", value=loss, step=global_step)

        all_shape_ious = []
        for cat in train_shape_ious.keys():
            for iou in train_shape_ious[cat]:
                all_shape_ious.append(iou)
            train_shape_ious[cat] = np.mean(train_shape_ious[cat])
        mean_train_shape_ious = np.mean(list(train_shape_ious.values()))

        print("Train mean loss: ", total_loss / total_seen)
        print("Train mean accuracy: ", total_correct / total_seen)
        print("Train Shape mIOU: ", mean_train_shape_ious)
        logger.info("Train mean loss: %f", total_loss / total_seen)
        logger.info("Train mean accuracy: %f", total_correct / total_seen)
        logger.info("Train Shape mIOU: %f", mean_train_shape_ious)
        experiment.log_metric(name="train/mIOU", value=mean_train_shape_ious, step=epoch)
        experiment.log_metric(name="train/mAcc", value=total_correct / total_seen, step=epoch)
        experiment.log_metric(name="train/mLoss", value=total_loss / total_seen, step=epoch)

        eval_shape_ious, mean_loss, mean_acc = evaluate(model.eval(), testdataloader, num_classes, test_dataset)
        print('\r Test %s: %f   ***  %s: %f' % (blue('mIOU'), eval_shape_ious, blue('Best mIOU'), best_tst_accuracy))
        print('\r Test %s: %f   ***  %s: %f' % (blue('mean loss'), mean_loss, blue('mean acc'), mean_acc))
        logger.info('Test mIOU: %f  *** Best mIOU: %f', eval_shape_ious, best_tst_accuracy)
        logger.info('\r Test %s: %f   ***  %s: %f' % (blue('mean loss'), mean_loss, blue('mean acc'), mean_acc))

        experiment.log_metric(name="test/mIOU", value=eval_shape_ious, step=epoch)
        experiment.log_metric(name="test/mAcc", value=mean_acc, step=epoch)
        experiment.log_metric(name="test/mLoss", value=mean_loss, step=epoch)

        if (eval_shape_ious >= best_tst_accuracy) and epoch > 5:
            best_tst_accuracy = eval_shape_ious
            logger.info('Save model...')
            save_checkpoint(
                global_epoch + 1,
                train_shape_ious,
                eval_shape_ious,
                model,
                optimizer,
                str(checkpoints_dir))
            print('Saving model....')
        global_epoch += 1
    print('Best mIOU: %f'%best_tst_accuracy)

    logger.info('End of training...')

def evaluate(model, dataloader, num_classes, dataset):
    seg_classes = dataset.seg_classes
    shape_ious = {cat:[] for cat in seg_classes.keys()}
    seg_label_to_cat = {} # {0:Airplane, 1:Airplane, ...49:Table}
    for cat in seg_classes.keys():
        for label in seg_classes[cat]:
            seg_label_to_cat[label] = cat   
    
    total_correct = 0
    total_seen = 0
    total_loss = 0
    for i, data in tqdm(enumerate(dataloader, 0), total=len(dataloader), smoothing=0.9):
        points, _, target = data
        points = points.transpose(2, 1)
        batch_size, _, num_points = points.shape
        points, target = points.cuda(), target.cuda()
        model = model.eval()
        xyz = points[:, :3, :]
        with torch.no_grad():
            pred = model(points, xyz)

        loss = F.nll_loss(F.log_softmax(pred, dim = 1), target)

        pred_np = pred.cpu().data.numpy()
        target_np = target.cpu().data.numpy()
        loss = loss.cpu().data.numpy()

        # Constrain pred to the groundtruth classes (selected by seg_classes[cat])
        cur_pred_logits = pred_np
        cur_pred_val = np.zeros((batch_size, num_points)).astype(np.int32)
        for i in range(batch_size):
            cat = seg_label_to_cat[target_np[i, 0]]
            logits = cur_pred_logits[i]
            cur_pred_val[i] = np.argmax(logits[seg_classes[cat], :], 0) + seg_classes[cat][0]

        correct = np.sum(cur_pred_val == target_np)
        total_correct += correct
        total_seen += (batch_size * num_points)
        total_loss += loss * (batch_size * num_points)

        for i in range(target_np.shape[0]):
            segp = cur_pred_val[i]
            segl = target_np[i]
            cat = seg_label_to_cat[segl[0]]
            part_ious = [0.0 for _ in range(len(seg_classes[cat]))]
            for l in seg_classes[cat]:
                if (np.sum(segl == l) == 0) and (np.sum(segp == l) == 0):
                    part_ious[l - seg_classes[cat][0]] = 1.0
                else:
                    part_ious[l-seg_classes[cat][0]] = np.sum((segl==l) & (segp==l)) / float(np.sum((segl==l) | (segp==l)))
            shape_ious[cat].append(np.mean(part_ious))

    all_shape_ious = []
    for cat in shape_ious.keys():
        for iou in shape_ious[cat]:
            all_shape_ious.append(iou)
        shape_ious[cat] = np.mean(shape_ious[cat])
    mean_shape_ious = np.mean(list(shape_ious.values()))

    return mean_shape_ious, total_loss / total_seen, total_correct / total_seen





if __name__ == '__main__':
    args = parse_args()
    main(args)
